#ifndef LLL_H
#define LLL_H

#include "Node.h"

class LLL{
  public:
    LLL(); // Initialize the data members
    ~LLL(); // Clear the list and delete all data

    Node * get(const unsigned int); // Return Node at N position
    Node * get(Node *, const unsigned int, const unsigned int);

    void deleteAll(); // Delete all the Nodes
    void deleteAll(Node *);

    void deleteAllVal(const int); // Delete all the Nodes with a given value
    void deleteAllVal(Node *, int);

    void deleteOneVal(const int); // Deletes the first instance of a value in the list
    void deleteOneVal(Node *, int);

    void deleteAt(const unsigned int); // Delete value at N position

    int getIndex(const int); // Return index of a Node based on its value
    int getIndex(Node *, unsigned int, int);

    void list(); // Lists all of the Nodes in order
    void list(Node *);

    void list_reverse(); // Lists all of the Nodes in reverse order
    void list_reverse(Node *);

    Node * pop_front(); // Remove and return the value of the head Node

    Node * pop_back(); // Remove and return the value of the last Node
    Node * pop_back(Node *);

    Node * pop_at(const unsigned int); // Remove and return value of Node at N position
    Node * pop_at(Node *, unsigned int, const unsigned int);

    unsigned int size(); // Returns number of Nodes in list
    unsigned int size(Node *);

  protected:
    Node * head;
};

class ordered_LLL : public LLL{
  public:
    void add(Node *);
    void add(Node *, Node *);

    unsigned int get_index(); // get index of a Node with a certain value
};

class unordered_LLL : public LLL{
  public:
    void add_beg(Node *); // Add new Node to beginning of list

    void add_end(Node *); // Add new Node to end of list
    void add_end(Node *, Node *);

    void add_at(Node *, unsigned int); // Add new Node at N position
    void add_at(Node *, Node *, unsigned int, unsigned int);
};

#endif
