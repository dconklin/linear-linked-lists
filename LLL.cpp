#include <iostream>
#include "LLL.h"
#include "Node.h"

/*
 * Initialize the key members
 */
LLL::LLL(){
  this->head = nullptr;
}

/*
 * Clear the list and delete all key
 */
LLL::~LLL(){
  deleteAll();
  delete(this->head);
}

/*
 * Return Node at N position
 */
Node * LLL::get(const unsigned int n){
  if(n < 0 || n >= size()) return nullptr;
  Node * return_Node = nullptr;
  if(this->head){
    if(n == 0) return_Node = this->head;
    else return_Node = get(this->head, 0, n);
  }
  return return_Node;
}
Node * LLL::get(Node * curr, unsigned int pos, unsigned int n){
  if(pos+1 == n) return curr->next;
  else return get(curr->next, ++pos, n);
}

/*
 * Delete all the Nodes
 */
void LLL::deleteAll(){
  if(this->head){
    deleteAll(this->head);
    this->head = nullptr;
  }
  return;
}
void LLL::deleteAll(Node * curr){
  if(curr){
    deleteAll(curr->next);
    delete(curr);
  }
  return;
}

/*
 * Delete all the Nodes with a given value
 */
void LLL::deleteAllVal(const int key){
  if(this->head){
    deleteAllVal(this->head, key);
    if(this->head->key == key){
      if(this->head->next){
        Node * new_head = this->head->next;
        delete(this->head);
        this->head = new_head;
      }else{
        delete(this->head);
        this->head = nullptr;
      }
    }
  }
  return;
}
void LLL::deleteAllVal(Node * curr, int key){
  if(curr->next->key == key){
    Node * del = curr->next;
    curr->next = (del->next) ? del->next : nullptr;
    if(curr->next) deleteAllVal(curr, key);
    delete(del);
  }else{
    if(curr->next->next) deleteAllVal(curr->next, key);
  }
  return;
}

/*
 * Deletes the first instance of a value in the list
 */
void LLL::deleteOneVal(const int key){
  if(this->head){
    if(this->head->key == key){
      if(this->head->next){
        Node * new_head = this->head->next;
        delete(this->head);
        this->head = new_head;
      }else{
        delete(this->head);
        this->head = nullptr;
      }
    }else deleteOneVal(this->head, key);
  }
  return;
}
void LLL::deleteOneVal(Node * curr, int key){
  if(curr->next->key == key){
    Node * del = curr->next;
    curr->next = (del->next) ? del->next : nullptr;
    if(curr->next) deleteOneVal(curr, key);
    delete(del);
  }else{
    if(curr->next->next) deleteOneVal(curr->next, key);
  }
  return;
}

/*
 * Delete value at N position
 */
void LLL::deleteAt(const unsigned int n){
  if(this->head){
    delete(pop_at(n));
  }
  return;
}

/*
 * Lists all of the Nodes in order
 */
void LLL::list(){
  if(this->head){
    std::cout << "The list is: ";
    list(this->head);
    std::cout << std::endl;
  }else std::cout << "The list is empty." << std::endl;
  return;
}
void LLL::list(Node * curr){
  if(curr){
    std::cout << curr->value << " ";
    list(curr->next);
  }
  return;
}

/*
 * Lists all of the Nodes in reverse order
 */
void LLL::list_reverse(){
  if(this->head){
    std::cout << "The reverse order of the list is: ";
    list_reverse(this->head);
    std::cout << std::endl;
  }else std::cout << "The list is empty." << std::endl;
}
void LLL::list_reverse(Node * curr){
  if(curr->next) list_reverse(curr->next);
  std::cout << curr->key << " ";
  return;
}

/*
 * Remove and return the value of the head Node
 */
Node * LLL::pop_front(){
  Node * return_Node = nullptr;
  if(this->head){
    return_Node = this->head;
    this->head = (this->head->next) ? this->head->next: nullptr;
    return_Node->next = nullptr;
  }
  return return_Node;
}

/*
 * Remove and return the value of the last Node
 */
Node * LLL::pop_back(){
  Node * return_Node = nullptr;
  if(this->head){
    return_Node = (this->head->next) ? pop_back(this->head) : pop_front();
    return_Node->next = nullptr;
  }
  return return_Node;
}
Node * LLL::pop_back(Node * curr){
  if(curr->next->next) return pop_back(curr->next);
  Node * return_Node = curr->next;
  curr->next = nullptr;
  return return_Node;
}

/*
 * Remove and return value of Node at N position
 */
Node * LLL::pop_at(const unsigned int n){
  if(n < 0 || n >= size()) return nullptr;
  Node * return_Node = nullptr;
  if(this->head){
    if(n == 0) return_Node = pop_front();
    else if(size() == n+1) return_Node = pop_back();
    else return_Node = pop_at(this->head, 0, n);
    return_Node->next = nullptr;
  }
  return return_Node;
}
Node * LLL::pop_at(Node * curr, unsigned int pos, const unsigned int n){
  Node * return_Node = nullptr;
  if(pos+1 == n){
    return_Node = curr->next;
    curr->next = (return_Node->next) ? return_Node->next : nullptr;
  }else return_Node = pop_at(curr->next, ++pos, n);
  return return_Node;
}

/*
 * Returns number of Nodes in list
 */
unsigned int LLL::size(){
  return size(this->head);
}
unsigned int LLL::size(Node * curr){
  if(curr) return size(curr->next) + 1;
  return 0;
}

/*
 * Return index of a Node based on its value
 */
int LLL::getIndex(const int val){
  return getIndex(this->head, 0, val);
}
int LLL::getIndex(Node * curr, unsigned int pos, int val){
  if(curr->key == val) return pos;
  else if(curr->next == nullptr) return -1;
  else return getIndex(curr->next, ++pos, val);
}

/*
 * Add new Node to ordered list
 */
void ordered_LLL::add(Node * new_Node){
  if(this->head){
    if(new_Node->key < this->head->key){
      new_Node->next = this->head;
      this->head = new_Node;
    }else if(this->head->next){
      add(this->head, new_Node);
    }else{
      this->head->next = new_Node;
    }
  }else{
    this->head = new_Node;
  }
  return;
}
void ordered_LLL::add(Node * curr, Node * new_Node){
  if(curr->next && curr->next->key < new_Node->key){
    add(curr->next, new_Node);
    return;
  }
  new_Node->next = curr->next;
  curr->next = new_Node;
}

/*
 * Add new Node to beginning of list
 */
void unordered_LLL::add_beg(Node * new_Node){
  new_Node->next = (this->head) ? this->head : nullptr;
  this->head = new_Node;
}

/*
 * Add new Node to end of list
 */
void unordered_LLL::add_end(Node * new_Node){
  if(this->head) add_end(this->head, new_Node);
  else this->head = new_Node;
}
void unordered_LLL::add_end(Node * curr, Node * new_Node){
  if(curr->next) add_end(curr->next, new_Node);
  else curr->next = new_Node;
}

/*
 * Add new Node at N position
 */
void unordered_LLL::add_at(Node * new_Node, unsigned int n){
  if(n < 0 || n > size()) return;
  if(n == 0){
    new_Node->next = (this->head) ? this->head : nullptr;
    this->head = new_Node;
  }else{
    add_at(this->head, new_Node, 0, n);
  }
  return;
}
void unordered_LLL::add_at(Node * curr, Node * new_Node, unsigned int pos, unsigned int n){
  if(pos+1 == n){
    new_Node->next = curr->next;
    curr->next = new_Node;
  }else add_at(curr->next, new_Node, ++pos, n);
  return;
}
