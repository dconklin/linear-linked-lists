#ifndef NODE_H
#define NODE_H

class Node{
  public:
    Node(int, int);
    int key;
    int value;
    Node * next;
};

#endif
