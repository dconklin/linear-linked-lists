* Linear Linked List Library
** About
This is a basic template for linear linked lists for my school projects, and has many useful functions implemented.
This is not meant to replace something like LinkedList in Java, but is meant to be easy to edit and implement.

** Installation
Just download or clone this repository into your project, and import the libraries using

#+begin_src cpp
#include "LLL.h"
#+end_src

** How To Use
*** Initialize List

#+begin_src cpp
unordered_LLL list;
ordered_LLL ordered_list;
#+end_src

*** Inserting Items

Start by creating your node, and then add:

#+begin_src cpp
Node * node = new Node(key, value);
#+end_src

Adding in unordered lists:

#+begin_src cpp
list.add_beg(node);
list.add_end(node);
list.add_at(node, 0);
#+end_src

Adding in ordered lists:

#+begin_src cpp
ordered_list.add(node);
#+end_src

*** Listing Items

#+begin_src cpp
list.list();
list.list_reverse();
#+end_src

*** Other Functions

#+begin_src cpp
list.size();
#+end_src

*** Deleting List

#+begin_src cpp
list.pop_front();
list.pop_back();
list.pop_at(0);
list.deleteAt(0);
list.deleteAll();
#+end_src

** Functions

LLL Functions
|-----------------------+---------------------------------------------------|
| Function              | Description                                       |
|-----------------------+---------------------------------------------------|
| Constructor           | Initialize the data members                       |
| Destructor            | Clear the list and delete all data                |
| get(int N)            | Return node at N position                         |
| deleteAll             | Delete all the nodes                              |
| deleteOneVal(int val) | Delete all the nodes with val value               |
| deleteAt(int N)       | Delete node at N position                         |
| getIndex(int val)     | Return index of node with val value               |
| list                  | list values in LLL                                |
| list reverse          | List values in LLL in reverse                     |
| pop front             | Remove and return value of the head node          |
| pop back              | Remove and return value of the tail node          |
| pop at(int N)         | Remove and return value of the node at N position |
| size                  | Returns size of list                              |
|-----------------------+---------------------------------------------------|

Unordered LLL Functions
|----------------------------+----------------------------------|
| Function                   | Description                      |
|----------------------------+----------------------------------|
| add beg(Node * node)       | Insert node at beginning of list |
| add end(Node * node)       | Insert node at end of list       |
| add at(Node * node, int N) | Insert node at N position        |
|----------------------------+----------------------------------|

Ordered LLL Functions
|------------------+--------------------------------|
| Function         | Description                    |
|------------------+--------------------------------|
| add(Node * node) | Insert node at sorted position |
|------------------+--------------------------------|
